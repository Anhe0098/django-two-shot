from django import forms


class LogInForms(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


class SignUpForms(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
